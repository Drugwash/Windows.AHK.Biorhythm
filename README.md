**Biorhythm** displays the biorhythmic chart of the selected person for a period of 31 subsequent days. The period can start with current date (TODAY) or can show the past 15 days and the future 15 days, with TODAY being shown in the middle of the chart. The display mode is selectable in the **main menu > Preferences > Past & future**.

The chart can also display the fully cycle set, or the minimal cycle set, according to the setting in **main menu > Preferences > Full cyle set**. The full set contains seven cycles:

- physical
- emotional
- intellectual
- intuition
- aesthetic
- awareness
- spiritual

![](./ss_biorhythm-full.png)

while the minimal set contains only the three most known cycles:

- physical
- emotional
- intellectual

![](./ss_biorhythm-simple.png)

The application remembers most recent person profiles added to the list, useful for families, work teams, or whatever other groups that may regularly check out their charts.		
Profiles can be loaded/added/edited/deleted in **main menu > File**.

**Critical days** are those when a cycle goes through the zero value either in descending or ascending mode.		
**Multi-critical** days are those when more than one cycle goes through zero at the same time.

Of them, the multi-critical days are to be most aware of, as the mind - and body - may behave in a slightly unusual way.

There is a more detailed help file bundled with the package, that can be accessed through the **main menu > Help > Details**.

***

The original version of this script has been built by **SoggyDog** at the old AutoHotkey forums. This here is a fork/continuation of his **[version](https://www.autohotkey.com/board/topic/28469-biorhythms-development-temporarily-suspended/)**.

***
The script uses **RMChart** by **Rainer Morgen** for displaying the chart. Separate licence for the RMchart library is provided as per its author's request:

**SOFTWARE LICENSE AGREEMENT**

This is an agreement between Rainer Morgen (the Licensor), and you. It covers your use of RMChart ("the Software"). Do not use the Software accompanying this Agreement until you have carefully read the following Agreement. Using the Software (or authorizing any other person to do so) indicates your acceptance of the terms and conditions contained in this Agreement. If you do not agree with the terms and conditions of this Agreement, promptly remove the Software from your computer.

You are granted a non-exclusive license to use the Software. This license grants you the right to use the Software on a royalty-free basis as freeware (without fee). You may freely distribute along with your application the files rmchart.dll and rmchart.ocx, as long as you  mention somewhere in your program and/or in the acompanying documents ("About", "Readme file" and so on), that your application uses RMChart.

The Software is protected by copyright laws. You may not decompile, reverse engineer, disassemble, or otherwise reduce the Software to human-perceivable form. You may not modify, adapt, translate, rent, sublicense, assign, lease, loan, resell for profit, distribute, or network the Software or create derivative works based upon the Software or any part thereof.

THIS SOFTWARE IS PROVIDED "AS-IS", WITHOUT A WARRANTY OF ANY KIND. ALL EXPRESS OR IMPLIED REPRESENTATIONS AND WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. IN NO EVENT SHALL THE LICENSOR OR CONTRIBUTORS BE LIABLE FOR ANY PROBLEMS OR DAMAGES OF ANY KIND (HARDWARE OR SOFTWARE, DIRECT OR INDIRECT, CAUSING A LOSS OF TIME, DATA, MONEY, OR ANYTHING ELSE) RESULTING FROM THE USE OF THE SOFTWARE.

                                                          
                                                         Rainer Morgen
                                                  http://www.rmchart.com
                                                         (C) 2004-2005

***

The application is standalone, meaning it doesn't need installation. At first run it will create a subfolder called **res** in the current folder, where the RMChart library is stored together with its license, help file and detailed Biorhythm help file. It is recommended therefore that the main Biorhythm executable be placed in its own folder in order to avoid clutter and/or confusion, **before** running it for the first time.		
Please make sure the location where the executable is placed does have write permissions for the current user.

Enjoy!

**© Drugwash, 2012-2023**